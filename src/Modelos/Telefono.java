/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import BaseDatos.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Usuario
 */
public class Telefono {
    
    Database conexion = new Database();
    Connection con = conexion.abrirConexion();

    ResultSet rs = null;
    Statement stmt = null;
    
  
    private String telefono;
    private int idCliente;
    
    
    public boolean nuevoTelefono(int idCliente,String telefono) {
        
        

      
        
        //verificamos si el cliente existe
        if (verificarExistencia(telefono) > 0) {
            JOptionPane.showMessageDialog(null, "el teléfono ya existe");
            return false;
        } else {

            try {
                conexion.abrirConexion();
                stmt = con.createStatement();
                
                String dir = "Insert Into telefono (telefono, idCliente) VALUES ('" + telefono + "','" + idCliente +  "')";
                stmt.executeUpdate(dir);

            } catch (Exception e) {

                System.err.println("ERROR AL INSERTAR: " + e);
            }

        }
        return true;
    }
    
    public int verificarExistencia(String telefono) {

        int contador = 0;

        try {
            conexion.abrirConexion();
            stmt = con.createStatement();
            String tel = "SELECT * FROM telefono WHERE telefono = '" + telefono + "'";
            rs = stmt.executeQuery(tel);

        } catch (SQLException e) {
            System.out.println("Error al Buscar " + e);
        }
        try {

            while (rs.next()) {

                contador++;

            }
        } catch (SQLException e) {
            System.out.println("Error al contar ");
        }

        return contador;
    }
    
     public void eliminarTodosLosTelefonos(int idCliente){
        try {
                conexion.abrirConexion();
                stmt = con.createStatement();
                
                String tel = "delete from telefono where idCliente = '" + idCliente + "'";
                stmt.executeUpdate(tel);

            } catch (Exception e) {

                System.err.println("ERROR AL ELIMINAR TODOS LOS TELEFONOS: " + e);
            }
        
        
        
    }
    
    
}
