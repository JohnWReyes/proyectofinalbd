/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Usuario
 */
public class SubOpcion {
    
    public int idSubOpcion;
    public Opcion idOpcion;
    public String nombre;

    public int getIdSubOpcion() {
        return idSubOpcion;
    }

    public void setIdSubOpcion(int idSubOpcion) {
        this.idSubOpcion = idSubOpcion;
    }

    public Opcion getIdOpcion() {
        return idOpcion;
    }

    public void setIdOpcion(Opcion idOpcion) {
        this.idOpcion = idOpcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
