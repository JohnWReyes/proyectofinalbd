/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import BaseDatos.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Usuario
 */
public class Restaurante {
    
    Database conexion = new Database();
    Connection con = conexion.abrirConexion();

    ResultSet rs = null;
    Statement stmt = null;
    public int idRestaurante;
    public String nombre;
    public String horario;
    public String direccion;
    public String ciudad;
   
    private String comparar;
    
     public boolean nuevoRestaurante(JTextField jTextCodigo,JTextField jTextNombre,JTextField jTextHorario,JTextField jTextDireccion ,JTextField jTextCiudad) {

        nombre = jTextNombre.getText();
        idRestaurante = Integer.parseInt(jTextCodigo.getText());
        direccion = jTextDireccion.getText();
        horario = jTextHorario.getText();
        ciudad = jTextCiudad.getText();
        
        if (verificarExistencia(idRestaurante) > 0) {
            JOptionPane.showMessageDialog(null, "El cliente ya existe");
            return false;
        } else {

            try {
                conexion.abrirConexion();
                stmt = con.createStatement();
                String sql1 = "Insert Into restaurante (clname,cldocument,cladress,cltelephon,clemail) VALUES ('" + nombre + "','" + idRestaurante + "','" + direccion + "','" + horario + "','" + ciudad + "')";
                stmt.executeUpdate(sql1);

            } catch (Exception e) {

                System.err.println("ERROR AL INSERTAR: " + e);
            }

        }
        return true;
    }

    public int verificarExistencia(int documento) {

        int contador = 0;

        try {
            conexion.abrirConexion();
            stmt = con.createStatement();
            String usu = "SELECT * FROM restaurante WHERE CLDOCUMENT = '" + documento + "'";
            rs = stmt.executeQuery(usu);

        } catch (SQLException e) {
            System.out.println("Error al Buscar " + e);
        }
        try {

            while (rs.next()) {

                contador++;

            }
        } catch (SQLException e) {
            System.out.println("Error al contar ");
        }

        return contador;
    }
    
     public void consulta1(JTextField tf_buscar) {

        comparar = tf_buscar.getText();
                try {
                    
                    conexion.abrirConexion();
                    
                    stmt = con.createStatement();
                    rs = stmt.executeQuery("SELECT * FROM restaurante WHERE cldocument like '%" + comparar + "%'" );
                    
                    System.out.println(rs.getString("cldocument")+", "+rs.getString("clname")+", "+rs.getString("cladress")+", "+ rs.getString("clpassword")+", " + rs.getString("cltelephon"));                

                    conexion.cerrarConexion();
                    
                } catch (SQLException e) {
                }
    }
     
     public void consultaDocumento(JTextField tf_buscar) {
        
        comparar = tf_buscar.getText();
        
                try {
                    
                    conexion.abrirConexion();
                    
                    stmt = con.createStatement();
                    rs = stmt.executeQuery("SELECT * FROM restaurante  where cldocument like '%" + comparar + "%' or clname like '%" + comparar + "%' or cladress like '%" + comparar + "%'" );
                    
                    System.out.println(rs.getString("cldocument")+", "+rs.getString("clname")+", "+rs.getString("cladress")+", "+ rs.getString("clpassword")+", " + rs.getString("cltelephon"));
 
                    
                    conexion.cerrarConexion();

                } catch (SQLException e) {
                }
    }
     
      public void borrarUsuario(JTextField jTextCodigo ) {

       
      

        try {

         
            stmt = con.createStatement();
            stmt.executeUpdate("DELETE  FROM restaurante WHERE cldocument='" + jTextCodigo + "' ");
        } catch (Exception e) {
            System.err.println("ERROR DE ELIMINAR CLIENTE " + e);
        }
    }
     
     public void actualizar2(JTextField jTextCodigo,JTextField jTextNombre,JTextField jTextHorario,JTextField jTextDireccion ,JTextField jTextCiudad) {

        String dato1 = "";
        String dato2 = "";
        String dato3 = "";
        String dato4 = "";
        String dato5 = "";

        

        dato1 = jTextNombre.getText().trim();
        dato2 = jTextNombre.getText().trim();
        dato3 = jTextHorario.getText().trim();
        dato4 = jTextDireccion.getText().trim();
        dato5 = jTextCiudad.getText().trim();

        
        System.out.println("QUE TRAE 1 :  " + dato2);
   

        try {
            conexion.abrirConexion();
          
            stmt.executeUpdate("UPDATE restaurante SET clname='" + dato1 + "',cldocument='" + dato2 + "',cladress='" + dato3 + "',cltelephon='" + dato4 + "',clemail='" + dato5 );
            
            
          
            
            

        } catch (Exception e) {
            System.err.println("ERROR AL ACTUALIZAR  " + e);
        }
    }
}

 
