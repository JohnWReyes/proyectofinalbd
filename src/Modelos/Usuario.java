/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import BaseDatos.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author John William Reyes
 * @author Diego Alejandro calvo
 */
public class Usuario {

    Database conexion = new Database();
    Connection con = conexion.abrirConexion();

    ResultSet rs = null;
    Statement stmt = null;
    
    private Direccion dir = new Direccion();
    private  Telefono tel = new Telefono();

    private int identificacion = 0;
    private String nombre = "";
    private String email = "";
    private String password = "";
    private String direccion = "";
    private String ciudad = "";
    private String telefono = "";
    private String rol = "";

    public boolean nuevocliente(JTextField jTextCodigo, JTextField jTextCorreo, JTextField jTextNombre, JTextField jTextPassword,JTextField jTextTelefono,JTextField jTextDireccion, JTextField jTextCiudad,JComboBox jComboBox1) {

        //Datos necesarios para crear un cliente en bases de datos
        nombre = jTextNombre.getText();
        identificacion = Integer.parseInt(jTextCodigo.getText());
        email = jTextCorreo.getText();
        password = jTextPassword.getText();
        rol = jComboBox1.getSelectedItem().toString();
        
        // Datos necesarios para crear una direccion
        direccion = jTextDireccion.getText();
        ciudad = jTextCiudad.getText();
        
        // Datos  necesarios para asignar un telefono
        telefono = jTextTelefono.getText();
        
        
        
        //verificamos si el cliente existe
        if (verificarExistencia(identificacion) > 0) {
            JOptionPane.showMessageDialog(null, "El cliente ya existe");
            return false;
        } else {

            try {
                conexion.abrirConexion();
                stmt = con.createStatement();
                String usuario = "Insert Into cliente (identificacion,nombre,email,password,rol) VALUES ('" + identificacion + "','" + nombre + "','" + email + "','" + password + "','" + rol + "')";                
                stmt.executeUpdate(usuario);

            } catch (Exception e) {

                System.err.println("ERROR AL INSERTAR: " + e);
            }
            
            try{
                dir.nuevaDireccion(identificacion, direccion, ciudad);
                
            }
            catch (Exception e){
                System.err.println("ERROR AL INSERTAR: " + e);
            }
             try{
                tel.nuevoTelefono(identificacion, telefono);
                
            }
            catch (Exception e){
                System.err.println("ERROR AL INSERTAR: " + e);
            }

        }
        return true;
    }

    public int verificarExistencia(int documento) {

        int contador = 0;

        try {
            conexion.abrirConexion();
            stmt = con.createStatement();
            String usu = "SELECT * FROM cliente WHERE identificacion = '" + documento + "'";
            rs = stmt.executeQuery(usu);

        } catch (SQLException e) {
            System.out.println("Error al Buscar " + e);
        }
        try {

            while (rs.next()) {

                contador++;

            }
        } catch (SQLException e) {
            System.out.println("Error al contar ");
        }

        return contador;
    }
    
     
    
      public void eliminarCliente(int identificacion ) {

       if(verificarExistencia(identificacion)==0){
           JOptionPane.showMessageDialog(null, "El cliente no existe");
           
       }
       else{
           try {

            dir.eliminarTodasLasDirecciones(identificacion);
            tel.eliminarTodosLosTelefonos(identificacion);
            stmt = con.createStatement();
            stmt.executeUpdate("DELETE  FROM cliente WHERE identificacion ='" + identificacion + "' ");
            JOptionPane.showMessageDialog(null, "Cliente Eliminado con éxito");
            
        } catch (Exception e) {
            System.err.println("ERROR Al ELIMINAR CLIENTE " + e);
        }
           
       }

        
    }
     
     public void actualizar(JTextField jTextCodigo, JTextField jTextCorreo, JTextField jTextNombre, JTextField jTextPassword, JTextField jTextTelefono) {

        String nombre = "";
        String email = "";
        String  password = "";
        String telefono = "";
        int  identificacion = 0;
        

        

        nombre = jTextNombre.getText().trim();
        password = jTextPassword.getText().trim();
        identificacion = Integer.parseInt(jTextTelefono.getText().trim());
        email = jTextCorreo.getText().trim();
        telefono = jTextTelefono.getText().trim();

        
        
   

        try {
            conexion.abrirConexion();
          
            stmt.executeUpdate("UPDATE cliente SET nombre='" + nombre + "', email='" + email + "', password='" + password );
            
            
          
            
            

        } catch (Exception e) {
            System.err.println("ERROR AL ACTUALIZAR  " + e);
        }
    }
     
    
}
