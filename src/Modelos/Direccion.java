/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import BaseDatos.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author John William Reyes
 * @author Diego Alejandro Calvo
 */
public class Direccion {
    
    Database conexion = new Database();
    Connection con = conexion.abrirConexion();

    ResultSet rs = null;
    Statement stmt = null;
    
    public int id;
    public String direccion;
    public String ciudad;
    
    
    public boolean nuevaDireccion(int idCliente,String direccion, String ciudad) {
         
        
        //verificamos si el cliente existe
        if (verificarExistencia(direccion) > 0) {
            JOptionPane.showMessageDialog(null, "la dirección ya existe");
            return false;
        } else {

            try {
                conexion.abrirConexion();
                stmt = con.createStatement();
                
                String dir = "Insert Into direccion (direccion,ciudad,idCliente) VALUES ('" + direccion + "','" + ciudad + "','" + idCliente + "')";
                stmt.executeUpdate(dir);

            } catch (Exception e) {

                System.err.println("ERROR AL INSERTAR: " + e);
            }

        }
        return true;
    }
    
    public int verificarExistencia(String direccion) {

        int contador = 0;

        try {
            conexion.abrirConexion();
            stmt = con.createStatement();
            String dir = "SELECT * FROM direccion WHERE direccion = '" + direccion + "'";
            rs = stmt.executeQuery(dir);

        } catch (SQLException e) {
            System.out.println("Error al Buscar " + e);
        }
        try {

            while (rs.next()) {

                contador++;

            }
        } catch (SQLException e) {
            System.out.println("Error al contar ");
        }

        return contador;
    }
    
    public void eliminarTodasLasDirecciones(int idCliente){
        try {
                conexion.abrirConexion();
                stmt = con.createStatement();
                
                String dir = "delete from direccion where idCliente = '" + idCliente + "'";
                stmt.executeUpdate(dir);

            } catch (Exception e) {

                System.err.println("ERROR AL ELIMINAR TODAS LAS DIRECCIONES: " + e);
            }
        
        
        
    }
    

    
    
    
   
    
    
}
