/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

/**
 *
 * @author Usuario
 */
public class Suscripcion {
    
    public Paquete idPaquete;
    public Restaurante idRestaurante;
    public Usuario idCliente;
    public double valorDisponible;

    public Paquete getIdPaquete() {
        return idPaquete;
    }

    public void setIdPaquete(Paquete idPaquete) {
        this.idPaquete = idPaquete;
    }

    public Restaurante getIdRestaurante() {
        return idRestaurante;
    }

    public void setIdRestaurante(Restaurante idRestaurante) {
        this.idRestaurante = idRestaurante;
    }

    public Usuario getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Usuario idCliente) {
        this.idCliente = idCliente;
    }

    public double getValorDisponible() {
        return valorDisponible;
    }

    public void setValorDisponible(double valorDisponible) {
        this.valorDisponible = valorDisponible;
    }
    
    
}
