/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Usuario
 */
public class Database {
    
    
 Connection con;
 Statement st;
 ResultSet rs;
 
 
 
 
public Connection abrirConexion()
{
try {
            String usuario = "root";
            String contraseña = "";
            String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://localhost:3306/restaurantedb";
            Class.forName(driver);
            con = DriverManager.getConnection(url,usuario, contraseña);
            if (con != null) {
                System.out.println("CONECTADO A LA BD");
            }

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("ERROR CONEXION" + ex);
        }
        return con;
 }
   

//Para cerrar la conexión una vez terminadas las consultas
 public void cerrarConexion() {
 try {
 con.close();
 System.out.println("Conexión cerrada");
 }
 catch (SQLException e) {
 System.out.println("Error al cerrar conexión");
   }
  }
 
 //Login envia a diferentes ventanas dependiendo si es cliente o restaurante
  public int login(JTextField jTextCorreo, JTextField jTextPassword) throws SQLException{
         
         String email = jTextCorreo.getText();
         String password = jTextPassword.getText();
         
         int identificacion = 0;
         String nombre = "";
         
         try {
            abrirConexion();
            st = con.createStatement();
            String usu = "SELECT * FROM cliente WHERE email = '" + email + "'";
            rs = st.executeQuery(usu);
            
            if (rs != null){
                while(rs.next()){ 
                if(rs.getString("password").equalsIgnoreCase(password)){
                    identificacion = Integer.parseInt(rs.getString("identificacion"));
                     nombre = rs.getString("nombre");
                     JOptionPane.showMessageDialog(null, "Bienvenido" +"  " + nombre);
                }
                else{
                    JOptionPane.showMessageDialog(null, "Usuario o contraseña inválidos");
                }
                
            }             
          }
            else{
                
                try{
            abrirConexion();
            st = con.createStatement();
            String rest = "SELECT * FROM restaurante WHERE email = '" + email + "'";
            rs = st.executeQuery(rest);
            
            if (rs != null){
                while(rs.next()){ 
                if(rs.getString("password").equalsIgnoreCase(password)){
                    identificacion = Integer.parseInt(rs.getString("identificacion"));
                     nombre = rs.getString("nombre");
                     JOptionPane.showMessageDialog(null, "Bienvenido" +"  " + nombre);
                }
                else{
                    JOptionPane.showMessageDialog(null, "Usuario o contraseña inválidos");
                }
                
            }             
            }
                    
                }catch (SQLException e) {
             
            System.out.println("Error al Buscar " + e);
        }
            }                  
            

        } 
         catch (SQLException e) {
             
            System.out.println("Error al Buscar " + e);
        }
         
         
        return identificacion;
         
     }

 
}

